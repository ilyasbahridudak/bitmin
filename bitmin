#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# 
# Author: Fuat Bölük <fuat@fuxproject.org>
#

import json
import os, sys, getopt, time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

"""
   Bitcoin mining sınıfı. https://btconline.io/ sitesi'ne bağlanıp
   cloudflare'yi 30 saniye bekliyor. Siteye girdikten sonra, bitcoin
   cüzdan adresinizi ve 4 haneli pin kodunu giriş alanlarına yazıp
   sisteme giriyor. Dashboard'daki mining yapılan btc sayacını 2 saniye
   ara ile terminal çıktısına yazıyor.

   İlk girişte'de doğru bir btc cüzdanı ile herhangi bir 4 haneli pin
   yazarak giriş sağlayabilirsiniz. ilk girişte kullandığınız pin kodunu
   unutmamalısınız. Artık o pin kodu ile giriş sağlayacaksınız.
"""
class btc():

    """
       Başlangıç fonksiyonu. Bu fonksiyon'da phantomjs sanal tarayıcı seçenekleri
       belirleniyor ve browser adında bir değişkene webdriver atanıyor.
    """
    def __init__(self):
        # tarayıcı seçenekleri javascript aktif, resimleri gösterme
        # firefox user-agent'i kullanıldı
        dcap = dict(DesiredCapabilities.PHANTOMJS)
        dcap['phantomjs.page.customHeaders.Accept-Language'] = "tr-TR, tr;q=0.9"
        dcap['phantomjs.page.customHeaders.Cache-Control'] = "max-age=0"
        dcap['phantomjs.page.settings.javascriptEnabled'] = True
        dcap['phantomjs.page.settings.loadImages'] = False
        dcap["phantomjs.page.settings.userAgent"] = (
             "Mozilla/5.0 (X11; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0"
        )

        # browser değişkeni'ne sanal tarayıcıyı ata
        self.browser = webdriver.PhantomJS(desired_capabilities=dcap)
        # Sanal tarayıcı ekran çözünürlüğü'nü 1366x768'e ayarla. Normal kullanıcı gibi görün :)
        self.browser.set_window_size(1366, 768)


    """
       Kontrol fonksiyonu. Bu kısımda Fux Projesi'nin resmi internet sitesin'de
       statik bir txt dosyası'na curl ile bağlantı sağlanıyor ve dosya okunuyor.
       Dosya içeriği TAMAM ise internet bağlantınızın var olduğu doğrulanıyor ve
       mining yapacak olan bitmin fonksiyonuna adresiniz ile pin kodunuzu iletiyor.
    """
    def control(self, wallet_address, pin):
        try:
            while True:
                # internete bağlan ve dosyayı oku. bağlantı varsa içinde TAMAM yazar.
                # bağlantı yoksa tarayıcı hatasını basacaktır. bu hata'da TAMAM ile eşleşmez.
                control = os.popen("curl -s https://fuxproject.org/static/hotspot.txt").read()

                # TAMAM'sa
                if control == "TAMAM":
                    # btc sayacı varsa ve okunan değer BTC ile bitiyorsa!
                    if self.browser.find_element_by_class_name("price").text.endswith("BTC") == True:
                        # mining devam ediyordur. remin fonksiyonuna yönlendir ve bu döngü'den çık.
                        self.remin(wallet_address, pin)
                        break
                    else:
                        # bitmin fonksiyonu'na adres ile pini gönder ve bu döngü'den çık.
                        self.bitmin(wallet_address, pin)
                        break
                else:
                    # Değilse 60 saniye bekle ve bu döngü'ye devam et.
                    time.sleep(60)
                    continue
        except:
            # Hata durumunda direk bitmin fonksiyonu'na yönlendir.
            self.bitmin(wallet_address, pin)


    """
       Bitmin fonksiyonu. Bu kısımda oluşturduğumuz sanal tarayıcı ile site adresine
       giriş yapıyoruz ve cloudflare'yi geçecek kadar bekliyoruz. Giriş sağlandıktan sonra
       bilgileri ilgili giriş alanlarına yazıp mining'i başlatıyoruz. Bu fonksiyonda 
       statik dosya bağlantısını yine internet bağlantınızı kontrol etmek maksadı ile kullanıyoruz. 
       Çıktıya btc sayaç değeri, saat, tarih, sayaç'ın dolar karşılığı ve sayaç'ın Türk lirası 
       karşılığını ekliyoruz.
    """
    def bitmin(self, wallet_address, pin):
        # Siteye git
        self.browser.get("https://btconline.io/")
        # 30 saniye bekle. cloudflare bazen 10 bazen 20 saniye üzeri bekliyor.
        time.sleep(30)
        # Name değeri wallet_address olan input'u seç ve cüzdan id'sini yapıştır.
        btc_addr = self.browser.find_element_by_name("wallet_address")
        btc_addr.send_keys(wallet_address)
        # Name değeri pin olan input'u seç ve pin kodunu yapıştır.
        btc_pin = self.browser.find_element_by_name("pin")
        btc_pin.send_keys(pin)
        # ENTER değeri göndererek start mining butonu'na tıklama oluştur.
        btc_pin.send_keys(Keys.RETURN)
        # Sayaç okuma'ya başlamadan önce 10 saniye bekle. eğer sayaç input'u yoksa hata verir.
        time.sleep(10)

        try:
            while True:
                # internete bağlan ve dosyayı oku. bağlantı varsa içinde TAMAM yazar.
                # bağlantı yoksa tarayıcı hatasını basacaktır. bu hata'da TAMAM ile eşleşmez.
                control = os.popen("curl -s https://fuxproject.org/static/hotspot.txt").read()

                # TAMAM'sa
                if control == "TAMAM":
                    # Sayaç değerini ve diğer bilgileri çıktıya yaz.
                    # 2 saniye bekle ve devam et. sayaç bilgileri güncellensin diye 2 saniye bekler.
                    self.btc_show(self.browser)
                    time.sleep(2)
                    continue
                else:
                    # Değilse! remin fonksiyonu'na bilgileri gönder ve bu döngü'den çık.
                    self.remin(wallet_address, pin)
                    break
        except:
             # Anlık olarak statik dosyalara ulaşamamış ve okuyamamış ise, internet bağlantısı ve
             # mining devam etmektedir. Bu nedenle remin fonksiyonu'na bilgileri yeniden yönlendir.
             # Çünki wallet ve pin inputu yoktur!. Giriş yapılmayacak!. Ancak giriş yapılması
             # gerekli ise hata verecektir. O nedenle bitmin fonksiyonu'na yönlendirilir.
             self.bitmin(wallet_address, pin)


    """
       Btc_show fonksiyonu. Bu kısımda sayaç değerini, saat ve tarih bilgisini anlık olarak
       alıp çıktıya yazıyoruz. Amacı while döngüsü içinde olması ve sürekli yeni değerleri
       tanımlayıp çıktıya yazmaktır. bitmin fonksiyonun'dan ayırmamızın sebebi ise döngüyü
       bağlantı yoksa tekrar kontrol fonksiyonu'na yönlendirdiğimiz kısmın daha okunaklı
       ve farkedilebilir olması içindir.
    """
    def btc_show(self, browser):
        try:
            # Tarih ve saati değişkenlere ata.
            date = time.strftime("%d.%m.%Y")
            clock = time.strftime("%H:%M:%S")
            # Sayaç değerini al.
            counter = self.browser.find_element_by_class_name("price").text.rstrip(" BTC")
            # Bitcoin'in Türk lirası ve Dolar karşılığını öğren
            data = os.popen("curl -s https://api.coinmarketcap.com/v2/ticker/1/?convert=TRY").read()
            # json'a çevir
            exchange = json.loads(data)
            # değerler...
            us = exchange["data"]["quotes"]["USD"]["price"]
            tr = exchange["data"]["quotes"]["TRY"]["price"]
            # Sayaç boş ise
            if counter == "":
                # Çıkış yap
                self.browser.get("https://btconline.io/signout")
                # Yeniden giriş için bitmin fonksiyonu'na yönlendir
                self.bitmin(wallet_address, pin)
            else:
                # Sayaç boş değilse
                new_us = us * float(counter)
                new_tr = tr * float(counter)
                # Değerleri çıktı'ya yaz. 85 karakter.
                print("Ƀ %s          %s          %s         $ %.2f         ₺ %.2f" % (counter, clock, date, new_us, new_tr), end="\n")
        except:
            # json dosyası adresten okunamadığın'da hata verirse tekrar btc_show fonksiyonu'nu çalıştır.
            self.btc_show(self.browser)


    """
       Remin fonksiyonu. Bu kısımda devam eden mining işlemini statik dosyalar ile internet
       bağlantısı'nın var olduğu anlaşılamaz ise giriş yapmadan devam etmemiz gerekiyor. Statik 
       dosyalar site'nin yayında olmaması, anlık down olması vs sebebiyle okunamamasın'dan 
       kaynaklanabilecek hatalarda yeniden yönlendirme ile mining'in durmaksızın devam 
       etmesini sağlıyoruz.
    """
    def remin(self, wallet_address, pin):
        # Siteye git
        self.browser.get("https://btconline.io/")
        # 10 saniye bekle. Sayaç ve site içeriği yüklensin.
        time.sleep(10)

        try:
            while True:
                # internete bağlan ve dosyayı oku. bağlantı varsa içinde TAMAM yazar.
                # bağlantı yoksa tarayıcı hatasını basacaktır. bu hata'da TAMAM ile eşleşmez.
                control = os.popen("curl -s https://fuxproject.org/static/hotspot.txt").read()

                # TAMAM'sa
                if control == "TAMAM":
                    # Sayaç değerini ve diğer bilgileri çıktıya yaz.
                    # 2 saniye bekle ve devam et. sayaç bilgileri güncellensin diye 2 saniye bekler.
                    self.btc_show(self.browser)
                    time.sleep(2)
                    continue
                else:
                    # Değilse! tekrar kontrol fonksiyonu'na bilgileri gönder ve bu döngü'den çık.
                    self.bitmin(wallet_address, pin)
                    break
        except:
             # Anlık olarak statik dosyalara ulaşamamış ve okuyamamış ise, internet bağlantısı ve
             # mining devam etmektedir. Bu nedenle remin fonksiyonu'na bilgileri yeniden yönlendir.
             # Ancak reminde'de hata verirse bitmin fonksiyonu'na yönlendirilmelidir. Hiçbiri 
             # işe yaramaz ise kontrol fonksiyonu'na yönlendirilir.
             self.control(wallet_address, pin)


    """
       Sınıfı işletecek olan ana fonksiyon. Bu kısımda, betiği başlatırken parametre olarak girilen
       seçenekleri ayıklıyoruz ve sınıfı başlatıyoruz. İstisnaları yakalamak amacıyla kullandığımız 
       try - except blokları dışında kalan ve hata verme olasılığı yüksek işlemler'den birinde bir 
       istisna veya hata oluşması halinde ve yeniden yönlendirilemediğinde Bu alanda sınıfı 
       başlattığımız için, oluşacak artık istisnaları'da yakalayabiliriz. Eğer hesaplanamayan bir 
       durumdan ötürü bir istisna oluşursa. bilgisayar arızası, elektrik kesintisi, hasar durumu veya 
       kullanıcı'nın betiği sonlandırması durumu hariç, durmaksızın mining yapması için, kontrol
       fonksiyonu'nu yine try - except bloğu içinde çağırıyoruz.
    """
    def main(self, argv):
        # Wal ve pin değeri öntanımlı olarak boş!
        # Eğer veri girişi olmamış ise betik çalışmayacak!
        wal = ""
        pin = ""

        try:
            opts, args = getopt.getopt(argv,"yc:p:",["cüzdan=","pin="])
        except getopt.GetoptError:
            print("bitmin -c <cüzdan_no> -p <pin>", end="\n")
            sys.exit(2)

        # Seçenekleri ayıkla
        for opt, arg in opts:
            if opt == "-y":
               print("bitmin -c <cüzdan_no> -p <pin>", end="\n")
               sys.exit()
            elif opt in ("-c", "--cüzdan"):
               wal = arg
            elif opt in ("-p", "--pin"):
               pin = arg
            else:
               print("Bilinmeyen Seçenek!", end="\n")
               sys.exit()

        # Cüzdan ve pin boş değilse!
        if wal != "" and pin != "":
            try:
                # Sınıfı başlat
                self.control(wal, pin)
            except:
                # Hata olursa yeniden başlat
                self.control(wal, pin)
         

if __name__ == "__main__":
    btc().main(sys.argv[1:])

# Son